@extends('shirt_layout')

@section('content')
    <?php

    $currency = Config::get('params.currency');
  //      $environment = 'development';
    $environment = 'live';

    if ($product->sale == 1 && $product->price > $product->salePrice) {
        $price = $product->salePrice;
    } else {
        $price = $product->price;
    }
    ?>

<style>
html body .container{ width:1420px;
  }

</style>

    <main id="page-content" class="tool-area pb30">

        


        <section class="tsc-area mb40 mt30 page--no-rwd">
	<div class="container">

		<div class="tsc-nav-area col-sm-3">
			
			<div class="tsc-nav nav1 mb40">
				<h3>Choose Product</h3>
				<ul class="nav navbar-nav">
					<li class="active"><a href="#"><i class="fa navicon1"></i> Products</a></li>
					<li class="dropdown">
					  <a href="#" class="dropdown-toggle" data-toggle="dropdown"><i class="fa navicon2"></i> Product Color   (30)</a></a>
					  <ul class="dropdown-menu">
						<button class="close fa fa-close"></button>
						<li>
							<h4>Current Color</h4>
							
							<div class="menu-plate color-plate list-col-4">
								
								<ul>
									<li><input type="radio" name="color" id="color_orange" /><label class="bg-orange" for="color_orange"></label></li>
									<li><input type="radio" name="color" id="color_red" /><label class="bg-red" for="color_red"></label></li>
									<li><input type="radio" name="color" id="color_blue" /><label class="bg-blue" for="color_blue"></label></li>
									<li><input type="radio" name="color" id="color_lime" /><label class="bg-lime" for="color_lime"></label></li>
									<li><input type="radio" name="color" id="color_green" /><label class="bg-green" for="color_green"></label></li>
									<li><input type="radio" name="color" id="color_purple" /><label class="bg-purple" for="color_purple"></label></li>
									<li><input type="radio" name="color" id="color_yellow" /><label class="bg-yellow" for="color_yellow"></label></li>
									<li><input type="radio" name="color" id="color_aqua" /><label class="bg-aqua" for="color_aqua"></label></li>
																			
								</ul>
							</div>
							
						</li>
					  </ul>
					</li>
				</ul>
			</div>
			
			
			<div class="tsc-nav nav1">
				<h3>Edit Artwork</h3>
				<ul class="nav navbar-nav">
					<li class="dropdown">
					  <a href="#" class="dropdown-toggle" data-toggle="dropdown"><i class="fa navicon3"></i> Artwork</a></a>
					  <ul class="dropdown-menu">
						<button class="close fa fa-close"></button>
						<li>
							<h4>Art Options</h4>
							
							<div class="menu-plate">
								<ul>
									<li><a href="#" data-toggle="modal" data-target="#modal__artwork">Design Ideas</a></li>
									<li><a href="#" data-toggle="modal" data-target="#modal__clipart" >Clips Art</a></li>
									<li><a href="#">My Saved Art</a></li>
								</ul>
							</div>
							
						</li>
					  </ul>
					</li>
					
					<li><a href="#" role="openinsidebar"><i class="fa navicon4"></i> Add Text</a></li>
					<li><a href="#" data-toggle="modal" data-target="#modal__uploadimage"><i class="fa navicon5"></i> Upload Image</a></li>
				</ul>
			</div>
			  
		</div>
		
		
		<div class="tsc-main col-sm-6">
		
			<div class="tsc-main__nav clrlist ">
				<ul>
					<li><button><span><img src="panel/images/icon-zoom.png" /><span>Zoom</button></li>
					<li><button><span><img src="panel/images/icon-selectall.png" /><span>Select All</button></li>
					<li><button><span><img src="panel/images/icon-duplicate.png" /><span>Duplicate</button></li>
					<li><button><span><img src="panel/images/icon-layer.png" /><span>Layers</button></li>
					<li><button><span><img src="panel/images/icon-undo.png" /><span>Undo</button></li>
					<li><button><span><img src="panel/images/icon-redo.png" /><span>Redo</button></li>
				</ul>
			</div>
			
			<div class="tsc-canvas">
				
				<div class="tsc-shirt text-center">
					<img src="panel/images/shirt.png" />
				</div>
				
				<div class="tsc-selection">
					<img src="panel/images/selection.png">
				</div>
			
			</div>
		
			<div class="tsc-main__actions text-center btn-effect--ripple">
				<button class="btn btn-default" disabled>SAVE & SHARE</button>
				<button class="btn btn-default">GET A QUOTE</button>
				<button class="btn btn-primary">CHECKOUT</button>
			
			</div>
			
		</div>
	
		<div class="tsc-sidebar col-sm-3">
		
			<div class="tsc-nav nav1">
				<h3>Print Locations</h3>
				
				<div class="sidebar__front">
					<h5>Front (2)</h5>
					<div class="sidebar__front__img">
						<img src="panel/images/shirt.png" alt="" />
					</div>
					<div class="form-group">
						<select class="form-control">
							<option>Front-Full</option>
							<option>Front-Full</option>
						</select>
					</div>
				</div>
				
				<div class="sidebar__back">
					<h5>Back (1)</h5>
					<div class="sidebar__back__img">
						<img src="panel/images/shirt.png" alt="" />
					</div>
					<div class="form-group">
						<select class="form-control">
							<option>Back-Full</option>
						</select>
					</div>
				</div>
				
			</div>
			
		</div>
	
	
	
	
	
	

	

<div class="modal modal--tsc fade" id="modal__uploadimage" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
        <h4 class="modal-title" id="myModalLabel">Upload Image</h4>
      </div>
      <div class="modal-body">
        
		<div class="upload__main p20">
		
		<div class="fom mb30"><input type="checkbox"> I agree to the terms and conditions of uploaded artwork. </div>
		
		<div class="bd mb30">
			<h5>SUPPORTED FILE TYPES</h5>
			<p>.png, .bmp. .jpg, .jpeg, .tif, .gif, .tiff, .pdf. .eps. .svg, .ai</p>
			<h5>25 MB MAX FILE SIZE</h5>
			<p>Depending on the file size some images may take a few minutes to upload</p>

				<div class="form-group">
					<input type="file" class="btn-primary" />
				</div>

		</div>		
		
		
				<div class="well">
					<h5>Privacy Notice</h5>
					Your uploaded images are safe, secure, and remain your propeny. By proceeding you acknowledge that you have full legal rights and permissions to use the images you upload. reserve the right to reject any order.
				</div>

      </div>
    </div>
  </div>
</div>

</div>


	
	
	
	

<div class="modal modal--tsc fade" id="modal__clipart" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
        <h4 class="modal-title" id="myModalLabel">Select Clipart</h4>
      </div>
      <div class="modal-body">
        
		<div class="modal__nav bg-gray listview p20">
		
		<div class="modal__search input-group">
		  <span class="input-group-addon"><button><i class="fa fa-search"></i></button></span>
		  <input type="text" class="form-control" placeholder="Username">
		</div>

		
				<ul class="nav navbar-nav">
					<li><a href="#">Athletic Dept.</a></li>
					<li><a href="#">Business</a></li>
					<li><a href="#">College & Campus</a></li>
					<li><a href="#">Events</a></li>
					<li><a href="#">Events & parties</a></li>
					<li><a href="#">First Responder</a></li>
					<li><a href="#">Fundraising</a></li>
					<li><a href="#">Holidays</a></li>
					<li><a href="#">Mascots</a></li>
					<li><a href="#">Military</a></li>
					<li><a href="#">Most Popular</a></li>
					<li><a href="#">Motorsports</a></li>
					<li><a href="#">Religious</a></li>
					<li><a href="#">School Activities</a></li>
					<li><a href="#">Slogans</a></li>
					<li><a href="#">Sports & Athletics</a></li>
					<li><a href="#">Trendy & Fashion</a></li>
				</ul>
				
				
		</div>
		
		<div class="modal__main list-col-4 list-artwork">
				<ul>
					
					<li><a href="#"><span><img src="panel/images/art6.jpg" /></span><h5>Artwork 6</h5></a></li>
					<li><a href="#"><span><img src="panel/images/art7.jpg" /></span><h5>Artwork 7</h5></a></li>
					<li><a href="#"><span><img src="panel/images/art8.jpg" /></span><h5>Artwork 8</h5></a></li>
					<li><a href="#"><span><img src="panel/images/art1.jpg" /></span><h5>Artwork 1</h5></a></li>
					<li><a href="#"><span><img src="panel/images/art2.jpg" /></span><h5>Artwork 2</h5></a></li>
					<li><a href="#"><span><img src="panel/images/art3.jpg" /></span><h5>Artwork 3</h5></a></li>
					<li><a href="#"><span><img src="panel/images/art4.jpg" /></span><h5>Artwork 4</h5></a></li>
					<li><a href="#"><span><img src="panel/images/art5.jpg" /></span><h5>Artwork 5</h5></a></li>
					<li><a href="#"><span><img src="panel/images/art6.jpg" /></span><h5>Artwork 6</h5></a></li>
					<li><a href="#"><span><img src="panel/images/art7.jpg" /></span><h5>Artwork 7</h5></a></li>
					<li><a href="#"><span><img src="panel/images/art8.jpg" /></span><h5>Artwork 8</h5></a></li>
					<li><a href="#"><span><img src="panel/images/art1.jpg" /></span><h5>Artwork 1</h5></a></li>
					<li><a href="#"><span><img src="panel/images/art2.jpg" /></span><h5>Artwork 2</h5></a></li>
					<li><a href="#"><span><img src="panel/images/art3.jpg" /></span><h5>Artwork 3</h5></a></li>
					<li><a href="#"><span><img src="panel/images/art4.jpg" /></span><h5>Artwork 4</h5></a></li>
					<li><a href="#"><span><img src="panel/images/art5.jpg" /></span><h5>Artwork 5</h5></a></li>
				</ul>		
		</div>		
		
      </div>
    </div>
  </div>
</div>











<div class="modal modal--tsc fade" id="modal__artwork" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
        <h4 class="modal-title" id="myModalLabel">Select Artwork</h4>
      </div>
      <div class="modal-body">
        
		<div class="modal__nav bg-gray listview p20">
		
		<div class="modal__search input-group">
		  <span class="input-group-addon"><button><i class="fa fa-search"></i></button></span>
		  <input type="text" class="form-control" placeholder="Username">
		</div>

		
				<ul class="nav navbar-nav">
					<li><a href="#">Athletic Dept.</a></li>
					<li><a href="#">Business</a></li>
					<li><a href="#">College & Campus</a></li>
					<li><a href="#">Events</a></li>
					<li><a href="#">Events & parties</a></li>
					<li><a href="#">First Responder</a></li>
					<li><a href="#">Fundraising</a></li>
					<li><a href="#">Holidays</a></li>
					<li><a href="#">Mascots</a></li>
					<li><a href="#">Military</a></li>
					<li><a href="#">Most Popular</a></li>
					<li><a href="#">Motorsports</a></li>
					<li><a href="#">Religious</a></li>
					<li><a href="#">School Activities</a></li>
					<li><a href="#">Slogans</a></li>
					<li><a href="#">Sports & Athletics</a></li>
					<li><a href="#">Trendy & Fashion</a></li>
				</ul>
				
				
		</div>
		
		<div class="modal__main list-col-4 list-artwork">
				<ul>
					<li><a href="#"><span><img src="panel/images/art1.jpg" /></span><h5>Artwork 1</h5></a></li>
					<li><a href="#"><span><img src="panel/images/art2.jpg" /></span><h5>Artwork 2</h5></a></li>
					<li><a href="#"><span><img src="panel/images/art3.jpg" /></span><h5>Artwork 3</h5></a></li>
					<li><a href="#"><span><img src="panel/images/art4.jpg" /></span><h5>Artwork 4</h5></a></li>
					<li><a href="#"><span><img src="panel/images/art5.jpg" /></span><h5>Artwork 5</h5></a></li>
					<li><a href="#"><span><img src="panel/images/art6.jpg" /></span><h5>Artwork 6</h5></a></li>
					<li><a href="#"><span><img src="panel/images/art7.jpg" /></span><h5>Artwork 7</h5></a></li>
					<li><a href="#"><span><img src="panel/images/art8.jpg" /></span><h5>Artwork 8</h5></a></li>
					<li><a href="#"><span><img src="panel/images/art1.jpg" /></span><h5>Artwork 1</h5></a></li>
					<li><a href="#"><span><img src="panel/images/art2.jpg" /></span><h5>Artwork 2</h5></a></li>
					<li><a href="#"><span><img src="panel/images/art3.jpg" /></span><h5>Artwork 3</h5></a></li>
					<li><a href="#"><span><img src="panel/images/art4.jpg" /></span><h5>Artwork 4</h5></a></li>
					<li><a href="#"><span><img src="panel/images/art5.jpg" /></span><h5>Artwork 5</h5></a></li>
					<li><a href="#"><span><img src="panel/images/art6.jpg" /></span><h5>Artwork 6</h5></a></li>
					<li><a href="#"><span><img src="panel/images/art7.jpg" /></span><h5>Artwork 7</h5></a></li>
					<li><a href="#"><span><img src="panel/images/art8.jpg" /></span><h5>Artwork 8</h5></a></li>
				</ul>		
		</div>		
		
      </div>
    </div>
  </div>
</div>






	</div>
</section>


    </main>
	
	
				

				
<script>

function qs(s) { return document.querySelector(s) }

var handle = qs('.fnc-range input[type="range"]');
var progressbar = qs('.fnc-range div[role="progressbar"]');

handle.addEventListener('input', function(){
  progressbar.style.width = this.value + '%';
  progressbar.setAttribute('aria-valuenow', this.value);
});



if (navigator.appName == 'Microsoft Internet Explorer' ||  !!(navigator.userAgent.match(/Trident/) || navigator.userAgent.match(/rv:11/)) || (typeof $.browser !== "undefined" && $.browser.msie == 1))
{
  jQuery("html").addClass("ie");
}

</script>
				
				
@include('front/product_tool_script')
@endsection
