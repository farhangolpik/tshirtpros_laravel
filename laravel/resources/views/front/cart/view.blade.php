@extends('layout')

@section('content')
<div id="fh5co-contact" class="__web-inspector-hide-shortcut__">
    <div class="container">
        <?php
        $currency = Config::get('params.currency');
        ?>
        <div class="row">
            <div class="container-fluid">
                <div class="checkout-back">
                    @if (count($cart)==0)
                    <div class="alert alert-success">
                        <h4><i class="icon fa fa-check"></i> &nbsp  Your Basket is empty</h4>
                    </div>
                    @endif
                    @if (count($cart)>0)
                    <div class="col-lg-8 col-md-8 col-sm-8">
                        <div class="check-left-sect">
                            <form id="cart_update" name="cart_update"  action="update" >
                                <table class="table">
                                    <thead>
                                        <tr>
                                            <td>Products</td>
                                            <td>Delete</td>
                                            <td>Qty</td>
                                            <td>Single</td>
                                            <td>Total</td>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <?php
                                        $sum = 0;


                                        ?>
                                        @foreach ($cart as $product)
                                        <?php
                                       // print_r($product->cart_customized_image); die;
                                        $rowTotal = $product->total_price * $product->quantity;
                                        $sum+=$rowTotal;
                                        ?>
                                        <tr>
                                            <td>
                                                <?php
                                                if ($product->simpleProduct == 0) {
                                                    ?>
                                                
                                                
                                                <!-- Trigger the Modal -->

<!-- The Modal -->
<div id="myModal" class="modal">

  <!-- The Close Button -->
  <span class="close" onclick="document.getElementById('myModal').style.display='none'">&times;</span>

  <!-- Modal Content (The Image) -->
  <img class="modal-content" id="img01">

  <!-- Modal Caption (Image Text) -->
  <div id="caption"></div>
</div>
                                                
                                                
                                                <!--<a download="<?php echo $product->cart_customized_image ?>" target="_blank" href="{{ asset('uploads/customize_products/large')}}/<?php echo $product->cart_customized_image ?>" title="ImageName">Download Large File</a>-->
                                                    <div class="checkout-img">

                                                        <img id="<?php echo $product->cart_customized_image ?>" onclick="preview(this)" src="{{ asset('uploads/customize_products/small')}}/<?php echo $product->cart_customized_image ?>" height="100px" width="100px" alt="<?php echo $product->product_name; ?>" />
                                                    </div>
                                                    <?php
                                                } else {
                                                    echo $product->product_name;
                                                }
                                                ?>
                                                <br clear="all" />
                                            </td>
                                            <td><div class="trash-icon"><a href="{{url('/cart/delete').'/'}}<?php echo $product->cart_id ?>"><i class="fa fa-trash fa-2x"></i></a></div></td>
                                            <td><input size="1" class="form-control qty-txt" name="qty[<?php echo $product->cart_id ?>]" value="<?php echo $product->quantity ?>" maxlength="2" /></td>
                                            
                                           <td><span class="txt-price">{{ $currency[Config::get('params.currency_default')]['symbol']}} <?php echo $product->total_price ?></span></td>
                                            <td><span class="txt-price">{{ $currency[Config::get('params.currency_default')]['symbol']}} <?php echo $rowTotal; ?></span></td>
                                        </tr>
                                        <tr>
                                            <td colspan=5 class="cart-dtl"><div class="cart_product_name">
                                                    <?php echo $product->product_name; ?>
                                                </div>
                                                <?php
                                                $attributes = explode(',', $product->attribute);
                                                $values = explode(',', $product->value);
                                                while (list($key, $attribute) = each($attributes) and list($key, $value) = each($values)) {
                                                    if ($value == '--')
                                                        continue;
                                                    ?>
                                                    <div class="cart_attribute_name">
                                                        <p ><?php echo $attribute; ?> : <?php echo $value ?></p>
                                                    </div>
                                                    <?php
                                                }
                                                ?></td>
                                        </tr>

                                        @endforeach
                                    </tbody>
                                </table>
								<div class="clrlist">
								
                                <ul class="checkout-btm-lnks signalized--hover">
                                    <li><a class="btn btn-primary " href="{!! url('/') !!}"><i class="fa fa-arrow-right"></i> <span>Continue Shoping</span></a></li>
                                    <li><a class="btn btn-primary"  href="javascript:void(0);" onclick="submitCart();"><i class="fa fa-refresh"></i> <span>Update Cart</span></a></li>
                                    <li><a class="btn btn-primary"  href="{!! url('checkout') !!}"><i class="fa fa-shopping-cart"></i> <span>Proceed to checkout</span></a></li>
                                </ul>
								</div>
                            </form >
                        </div>

                        <table width="100%" border="0" id="cart_upselling"><tbody>

                                @foreach ($products as $product)
                                <tr>
                                    <td></td>
                                    <td>
                                        <?php echo $product->name ?>
                                        <form action="../cart/addsimple" method="get">
                                            <input type="hidden" name="total_price" value="<?php echo $product->price ?>">
                                            <input type="hidden" name="return" value="cart/view"/>
                                            <input type="hidden" name="price"  id="price" value="<?php echo $product->price; ?>" />
                                            <input type="hidden" name="quantity" value="1"/>
                                            <input type="hidden" name="product_id"  id="product_id" value="<?php echo $product->id; ?>" />							
                                            <p><strong>{{ $currency[Config::get('params.currency_default')]['symbol']}}<?php echo $product->price ?></strong> &nbsp; 
                                                <input type="submit" class="button" value="Add to Cart"/></p>
                                        </form>
                                    </td>
                                </tr>
                                @endforeach
                            </tbody></table>
                    </div>
                    <div class="col-lg-4 col-md-4 col-sm-4">
                        @include('front/orders/right')

                    </div>
                    @endif
                </div>
            </div>
        </div>
    </div>
</div>
<script>

    function submitCart() {
        $('#cart_update').submit();
    }
    
    
    // Get the modal
var modal = document.getElementById('myModal');

// Get the image and insert it inside the modal - use its "alt" text as a caption
var img = document.getElementById('myImg');
var modalImg = document.getElementById("img01");
var captionText = document.getElementById("caption");

function preview1(img){
    
    var image_path = '{{ asset('uploads/customize_products/large')}}'+'/'+img.id;
    modal.style.display = "block";
    modalImg.src = image_path;
    captionText.innerHTML = this.alt;
}

// Get the <span> element that closes the modal
var span = document.getElementsByClassName("close")[0];

// When the user clicks on <span> (x), close the modal
span.onclick = function() { 
  modal.style.display = "none";
}
</script>
@endsection
