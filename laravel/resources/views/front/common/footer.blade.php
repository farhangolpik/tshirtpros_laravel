<footer>

  <section class="ftr-top col-sm-12 p0">
    <div class="container">

          <div class="ftr-top-item col-sm-4">
            <div class="ftr__top__item__inr clrlist text-center">

              <ul>
                <li> <img src="{{asset('')}}/front/images/stndrd-shipng.png" /> </li>
                <li>
                  <h3> standard shipping </h3></li>
                <li> Normally in 14 Business Days </li>
                <li> FREE SHIPPING on orders over $100 </li>
              </ul>

            </div>
          </div>

          <div class="ftr-top-item col-sm-4">
            <div class="ftr__top__item__inr clrlist text-center">

              <ul>
                <li> <img src="{{asset('')}}/front/images/rush-shipng.png" /> </li>
                <li>
                  <h3> RUSH SHIPPING </h3></li>
                <li> 7 Business Days </li>
                <li> 7 DAYS Faster </li>
              </ul>


            </div>
          </div>

          <div class="ftr-top-item col-sm-4">
            <div class="ftr__top__item__inr clrlist text-center">

              <ul>
                <li> <img src="{{asset('')}}/front/images/support.png" /> </li>
                <li>
                  <h3> ONLINE SUPPORT </h3></li>
                <li> We support online 24 hours a </li>
                <li> day</li>

              </ul>

            </div>
          </div>

    </div>
  </section>


  <section class="ftr-btm col-sm-12 p0">


    <div class="container">


          <div class="ftr__box contact-us col-sm-4">
            <div class="contact__us__inr clrlist">
			<h4>Contact Us</h4> 
              <ul>
                <li>
                  <a> <img src="{{asset('')}}/front/images/ftr-addres.png" /> <span>Address: 123 PP Street, New York</span> </a>
                </li>
                <li>
                  <a> <img src="{{asset('')}}/front/images/ftr-phn.png" /> <span>Call Us: +012 345 6789</span> </a>
                </li>
                <li>
                  <a> <img src="{{asset('')}}/front/images/ftr-msg.png" /> <span>Email Us: Info@Company.com</span> </a>
                </li>
                <li>
                  <ul>
                    <li>
                      <a> <img src="{{asset('')}}/front/images/fb-icon.png" /></a>
                    </li>
                    <li>
                      <a> <img src="{{asset('')}}/front/images/Twitter-icon.png" /></a>
                    </li>
                    <li>
                      <a> <img src="{{asset('')}}/front/images/Gmail-icon.png" /></a>
                    </li>
                    <li>
                      <a> <img src="{{asset('')}}/front/images/Insta-icon.png" /></a>
                    </li>
                  </ul>
                </li>

              </ul>
            </div>
          </div>


          <div class="ftr__box info col-sm-4">
            <div class="info__inr clrlist">
				<h4>Other Info</h4>
              <ul>
                <li>
                  <p>At T-shirtPros, we're good at two things: Having fun and creating t-shirts. Strong believers in a "work hard, play hard" philosophy, we spend time perfecting our printing processes, bending over backward for our customers</p>
                </li>
                <li><img src="{{asset('')}}/front/images/cards.png" /></li>
              </ul>

            </div>
          </div>

          <div class="ftr__box newsltr col-sm-4">
            <div class="newsltr__inr clrlist">
				<h4>Subscribe Newsletter</h4>
              <ul>
                <li>
                  <div class="news-btn col-sm-12 pl0">
                    <input type="text" name="search" placeholder="Enter your email.....">
                    <button href="#"><i class="fa fa-paper-plane-o fa-2x" aria-hidden="true"></i></button>
                  </div>
                </li>
            </div>
          </div>

        </div>
     
    </div>
  </section>

  <section class="copyright-area bg-gray white p20 col-sm-12 a">
    <div class="container">

      <div class="ftr-copyright col-sm-12 ">
          <div class="pul-lft">Copyright &#9400; 2017 &nbsp; T-shirt Pros. All Right Reserved.</div>
		  <div class="pul-rgt">Designed & Developed by <span><a href="http://www.golpik.com">GOLPIK</a></span>. </div>
      </div>

    </div>
  </section>
 </footer>

 <script type="text/javascript">
$('#subscribe').click(function(){
$('form').submit(function(){
  var postData = $(this).serialize();
  
  $.ajax({
    type: 'POST',
    data: postData,
    dataType:'json',
    url: '<?php echo url('newsletter/store'); ?>',
    success: function(data){
      if (data == 1) 
      {
      $('#msg').html('Successfully Submitted!');
      alert('Successfully Submitted!');
      window.location.reload();
      }
      else
      {
      $('#msg').html(data.email[0]);  
      alert('Problem with your input');
      window.location.reload();
      }
    },
    error: function(data){
      console.log(data);
      alert('There was an error adding your comment');
    }
  });
  
  return false;
});
});
</script>
	
	