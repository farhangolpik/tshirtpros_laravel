<?php
$cart = Session::get('cart');
$categories = App\Categories::where("status", "=", 1)->get();
?>

<header>
	
	@include('front/common/header')

	<section class="hdr-area hdr-nav hdr--sticky cross-toggle bg-white" data-navitemlimit="7">
      <div class="container pl0 pr0">
        <nav class="navbar navbar-default" role="navigation" id="slide-nav">
          <div class="container-fluid pr0">
            <!-- Brand and toggle get grouped for better mobile display -->
            <div class="navbar-header">
              <button type="button" class="navbar-toggle collapsed slide-navbar" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
				<span class="sr-only">Toggle navigation</span>
				<span class="icon-bar"></span>
				<span class="icon-bar"></span>
				<span class="icon-bar"></span>
			  </button>

            </div>

            <!-- Collect the nav links, forms, and other content for toggling -->
            <div id="slidemenu">
              <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                <ul class="nav navbar-nav navbar-main navbar-move">
                  <li><a href="index.php">Home</a></li>
                  <li><a href="categories.php">Brands</a></li>
                  <li><a href="">Custom Apparel</a></li>
                  <li><a href="">Locations</a></li>
                  <li><a href="">Custom T-Shirts</a></li>
                  <li><a href="">Custom Hoodies</a></li>
                  <li><a href=""> Custom Ladies Apparel</a></li>
                </ul>

              </div>
              <!-- /.navbar-collapse -->
            </div>
          </div>
          <!-- /.container-fluid -->
        </nav>
      </div>
    </section>
</header>