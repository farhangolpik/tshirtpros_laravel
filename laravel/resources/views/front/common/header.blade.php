
	<section class="top-bar--section">
      <div class="container pl0">
        <div class="top-bar">
          <div class="top-bar-inner">

            <div class="top-bar--lft">
              <ul>
                <li><a href="#"><i class="fa fa-mobile fa-2x"></i> <span>+1 (000) 123-4567</span></a></li>
              </ul>
            </div>
			
			<div class="top-bar--rgt clrlist">
              <ul>
                <li><a href="contact.php">Contact Us</a></li>
				<?php
                                if (isset(Auth::user()->id)) {
                                    ?>
									<li class="dropdown" ><a href="cart.php" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="true">My Account</a>
										<ul class="dropdown-menu">
                                            <li ><a href="{{ url('myorders')}}" class="pagelinkcolor">My Orders</a></li>
                                            <li><a href="{{ url('changepassword')}}" class="pagelinkcolor">Change Password</a></li>
                                            <li><a href="{{ url('profile')}}" class="pagelinkcolor">Profile</a></li>
                                            <li><a href="{{ url('auth/logout')}}" class="pagelinkcolor">Log Out</a></li>
                                        </ul>
									</li>
									<li><a href="#">My Wishlist</a></li>
                <?php
                                } else {
                                ?><li><a href="{{url('')}}/signup">Sign In</a></li>
								<?php
                                }
                                ?>
				<li class="dropdown">
                  <a href="#" class="dropdown-toggle" data-toggle="dropdown">Us Dollar</a>
                  <ul class="dropdown-menu top">
                    <li><a href="#">USD $</a></li>
                    <li><a href="#">EURO &euro;</a></li>
                  </ul>
                </li>
				
              </ul>

            </div>
			
		</div>	
		</div>
	</section>
	<section class="nav-hdr--middle">
      <div class="container pl0 pr0">
        <div class="nav-hdr--middle col-sm-12">
          <div class="nav__hdr__middle__inner">
            <div class="col-sm-3 ml20 logo-area">
              <a class="navbar-brand" href="index.php"><img src="{{asset('')}}/front/images/logo.png" alt="logo" /></a>
            </div>

            <div class="search-btn col-sm-6 ml40 mt10">
              <input type="text" name="search" placeholder="Search Entire Store Here.....">
              <button href="#">SEARCH</button>
            </div>

            <div class="shpng-cart col-sm-2 pr0 ml30 mt10">
              <ul>
                <li><i class="fa fa-cart fa-3x" aria-hidden="true"></i><span>0</span></li>
                <li>
                  <ul>
                    <li>
                      <p class="mb0">shopping cart</p>
                    </li>
                    <li>
                      <p class="mb0">$ 0.00</p>
                    </li>
                  </ul>
                </li>
              </ul>

            </div>


          </div>

        </div>
      </div>
      </div>
    </section>
