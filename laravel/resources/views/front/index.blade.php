@extends('layout')
<?php
$title = 'Home';
$description = '';
$keywords = '';
?>
@include('front/common/meta')
@section('content')

<section class="slider-area hover-ctrl fadeft">
  <div id="carousel-example-generic" class="carousel slide slider--pauseplay" data-ride="carousel" data---interval="false">

    <ol class="carousel-indicators thumbs">
      <li data-target="#carousel-example-generic" data-slide-to="0" class="active"></li>
      <li data-target="#carousel-example-generic" data-slide-to="1"></li>
      <li data-target="#carousel-example-generic" data-slide-to="2"></li>
    </ol>

    <div class="carousel-inner">
      <div class="item active bg-cvr anime-down" style="background-image:url('{{asset('')}}front/images/banner-img.jpg');">
        <div class="caro-caps0">
          <div class="container">
            <div class="slide__cont col-sm-7 valigner ml10 anime-up delay1s">
              <div class="valign">
                <h1>DESIGN CUSTOM <span class="clr-shrt--bg">T-SHIRTS</span></h1>
                <h2>Meet your new favorite tee choose from 100s of styles.
	       Free Shipping US Wide, Order Yours Today</h2>
                <ul>
                  <li>T-shirt</li>
                  <li>Tanks</li>
                  <li>Hoodies</li>
                </ul>
                <div class="lnk-btn view-btn">
                  <button href="#">View Store</button>
                </div>

              </div>
            </div>


          </div>
        </div>
      </div>
    </div>

    <!--  <div class="carousel-inner">
		<div class="item active">
		  <img src="images/banner-img.jpg" alt="...">
		  <div class="container posrel">
			  <div class="caro-caps">
				<h1>DESIGN CUSTOM <span class="clr-shrt--bg">T-SHIRTS</span></h1>
				<h2>Meet your new favorite tee choose from 100s of styles.
					Free Shipping US Wide, Order Yours Today</h2>
				<ul>
				<li>T-shirt</li>
				<li>Tanks</li>
				<li>Hoodies</li>
				</ul>
				<div class="lnk-btn view-btn">
				<button href="#">View Store</button>
				</div>
			  </div>
		  </div>
		</div>

	  </div> -->
    <!--
	  <a class="left carousel-control" href="#carousel-example-generic" data-slide="prev">
		<span class="glyphicon glyphicon-chevron-left"></span>
	  </a>
	  <a class="right carousel-control" href="#carousel-example-generic" data-slide="next">
		<span class="glyphicon glyphicon-chevron-right"></span>
	  </a> -->


  </div>

</section>

<section class="slider-bg btm-section">
  <div class="container">
    <div class="slider-bg btm-area col-sm-12">
      <div class="slider__bg btm__area__inner">

        <div class="slider__btm__cont col-sm-8">
          <div class="slider__btm__cont__inner">
            <p>Have a great idea? T-shirtPros makes your design easily.</p>
          </div>
        </div>

        <div class="lnk-btn design-btn col-sm-3">
			<a href="product_tool.php">START DESIGNING</a>
        </div>


      </div>
    </div>
  </div>
</section>

<section class="abt-us-section" style="background-image:url('{{asset('')}}/front/images/abt-bg.jpg');">
  <div class="container">
    <div class="abt-area">
      <div class="abt__area__inner">


        <div class="abt-cont-area col-sm-7">
          <div class="abt__cont__inner">
            <div class="hed zigzag text-left"><h2>About us</h2></div>
            
            <p>When it comes to T-shirtPros, there's no denying it: you're obsessed. We understand how important your T-shirt project is to you. That's why we've established exactingstandards for print quality and offer multiple printing technologies to
              ensure that your order will come out great. No matter how your T-shirts are printed, They are sure to bring your group together. Custom T-shirts have the power to create lasting memories, start new traditions, and unite groups for a common
              purpose. We’ll take care of the printing so you and your group can show off your awesome T- shirts together!</p>

          </div>
        </div>

        <div class="abt-item-img col-sm-5">
          <div class="abt__item__img__inner">
            <img src="{{asset('')}}/front/images/abt-img.png" />
          </div>
        </div>


      </div>
</section>



	<section class="feature-item-section">
		<div class="container">
			
			<div class="hed zigzag">
			  <h2>Featured Items</h2>
			</div>

			<div class="feature-item-sub-title">
			  <p>The easiest way to order custom t-shirts online</p>
			</div>
			
			<div class="row">
			
			
			
			
			 @include('front.products.categories')
			 
			
			</div>
			
			
		</div>
	</section>
	
		
<section class="custom-apparel" style="background:url('{{asset('')}}/front/images/custom-bg.jpg')">
  <div class="container">
    <div class="custom">
      <div class="custom__inr">

        <div class="hed black text-center zigzag color-invert">
          <h2>Source of Custom Apparel</h2>
        </div>

        <div class="custom-sub-title">
          <p>The easiest way to order custom t-shirts online</p>
        </div>
      </div>
    </div>

    <div class="custom-item col-sm-12">
      <div class="item__inr">

        <div class="item1-img col-sm-4">
          <img src="{{asset('')}}/front/images/apparel-item1.png" />
        </div>

        <div class="item1-desc col-sm-4">
          <div class="item1__desc__inner">
            <div class="item1__desc__title">
              <h1>01</h1>
              <h2>Choose a Product</h2>
            </div>
            <div class="item1__desc__cont">
              <p>We have 100+ different products to choose from including t-shirts, tank tops, hoodies, long sleeves, jerseys and more.</p>
            </div>
          </div>
        </div>

      </div>
    </div>


    <div class="custom-item col-sm-5 col-sm-offset-7">
      <div class="item__inr">

        <div class="item2-desc col-sm-12">
          <div class="item2__desc__inner">
            <div class="item2__desc__title">
              <h1>02</h1>
              <h2>CUSTOMIZED <br/>iT!</h2>
            </div>
            <div class="item2__desc__cont">
              <p>Upload your own artwork or create a design online. Our designer makes it really easy to create custom apparel.</p>
            </div>
          </div>
        </div>

        <div class="item2-img col-sm-10 col-sm-offset-2">
          <img src="{{asset('')}}/front/images/apparel-item2.png" />
        </div>


      </div>
    </div>

    <div class="custom-item col-sm-12">
      <div class="item__inr">

        <div class="item3-img col-sm-12">
          <img src="{{asset('')}}/front/images/apparel-item3.png" />
        </div>

        <div class="item3-desc col-sm-12">
          <div class="item3__desc__inner">
            <div class="item3__desc__title col-sm-2 pr0 pl0">
              <h1>03</h1>
              <h2>CHECKOUT</h2>
            </div>
            <div class="item3__desc__cont col-sm-4 pl0">
              <p>Your order will be printed in US with the highest standards for quality, and the delivery date is fully guaranteed!</p>
            </div>
          </div>
        </div>
      </div>
    </div>

  </div>
</section>		

		



@endsection
