@extends('shirt_layout')

@section('content')
    <?php

    $currency = Config::get('params.currency');
  //      $environment = 'development';
    $environment = 'live';

    if ($product->sale == 1 && $product->price > $product->salePrice) {
        $price = $product->salePrice;
    } else {
        $price = $product->price;
    }
    ?>

<style>
html body .container{ width:1420px;
  }

</style>

    <main id="page-content" class="tool-area pb30">

        <section class="hdr-area white">
            <div class="container posrel">
                <div class="top-area col-sm-12 p0">
                    <div class="hed clrhm mt10 mb10">
						<!--<span class="logo"><a class="navbar-brand" href="{{url('')}}/"><img src="{{asset('')}}/front/images/logo.png" alt="logo"/></a> </span>-->
						<h3 class="p5">CUSTOMIZATION TOOL</h3></div>
                    @if($environment == 'development')
                        <div class="col-sm-5 pt10">
                            <button class="btn green_back" type="button" name="stroke" onclick="showShirtCanvas()">
                                SHIRT
                            </button>
                            <button class="btn green_back" type="button" name="stroke" onclick="showIphoneCanvas()">
                                IPHONE
                            </button>
                            <button class="btn green_back" type="button" name="stroke"
                                    onclick="showBuisnessCardCanvas()">BUSINESS CARD
                            </button>
                            <button class="btn green_back" type="button" name="stroke" onclick="showMugCanvas()">MUG
                            </button>
                        </div>

                    @endif

                </div>
            </div>
        </section>


        <section class="main-area">
            <div class="container ">


                <div class="main__tool tool-area col-xs-4 p0 bg-white">

                    <ul class="nav nav-tabs">
                        <li><a href="#tab1" data-toggle="tab" class="">Items</a></li>
                        <li class="active"><a href="#tab2" data-toggle="tab" class="">Text Editor</a></li>
                        @if($environment == 'development')
                            <li><a href="#tab3" data-toggle="tab" class="">Save & Load</a></li>
                        @endif
                    </ul>

                    <div class="tab-content">

                        <div class="tab-pane overload accordion-arr" id="tab1">


                            <div class="panel-group" id="accordion">


                                @if($environment == 'development')
                                <div class="panel panel-default">
                                    <!--<div class="panel-heading green_back">-->
                                    <!--<h4 class="panel-title">-->
                                    <!--<a data-toggle="collapse" data-parent="#accordion" href="#collapse1">-->
                                    <!--<i class="fa fa-github-alt"></i> Animal Prints-->
                                    <!--</a>-->
                                    <!--</h4>-->
                                    <!--</div>-->
                                    <div id="collapse1" class="panel-collapse collapse ">
                                        <div class="panel-body">


                                            <div class="element-area clrboth list-col-4">
                                                <ul>
                                                    <li><a href="#" onclick="return addAnimalImage('cat');"><img
                                                                    src="images/animal/cat.png" alt=""/></a></li>
                                                    <li><a href="#" onclick="return addAnimalImage('tiger');"><img
                                                                    src="images/animal/tiger.png" alt=""/></a></li>
                                                    <li><a href="#" onclick="return addAnimalImage('horse');"><img
                                                                    src="images/animal/horse.png" alt=""/></a></li>

                                                </ul>
                                            </div>


                                        </div>
                                    </div>
                                </div>

                                <div class="panel active panel-default">
                                    <div class="panel-heading green_back">
                                        <h4 class="panel-title">
                                            <a data-toggle="collapse" data-parent="#accordion" href="#collapse2">
                                                <i class="fa fa-wpforms"></i> Colored Icons
                                            </a>
                                        </h4>
                                    </div>
                                    <div id="collapse2" class="panel-collapse collapse in">
                                        <div class="panel-body">

                                            <!--<div class="form-group col-sm-6 p0">-->
                                            <!--<select class="form-control" disabled>-->
                                            <!--<option value="abc">Company</option>-->
                                            <!--<option value="abc">Raster</option>-->
                                            <!--</select>-->
                                            <!--</div>-->


                                            <div class="element-area clrboth list-col-4">
                                                <ul>
                                                    <!--<li><a href="#" onclick="return fbs_click(this);"><img src="images/logo/golpik.png" alt="" /></a></li>-->
                                                    <!--<li><a href="#" onclick="return addLogoImage('google');"><img src="images/logo/google.png" alt="" /></a></li>-->
                                                    <!--<li><a href="#" onclick="return addLogoImage('microsoft');"><img src="images/logo/microsoft.png" alt="" /></a></li>-->
                                                    <!--<li><a href="#" onclick="return addLogoImage('paypal');"><img src="images/logo/paypal.png" alt="" /></a></li>-->
                                                    <!--<li><a href="#" onclick="return addLogoImage('logo_2');"><img src="images/logo/logo_2.png" alt="" /></a></li>-->
                                                    <!--<li><a href="#" onclick="return addLogoImage('logo_1');"><img src="images/logo/logo_1.png" alt="" /></a></li>-->
                                                    <!--<li><a href="#" onclick="return addLogoImage('logo_3');"><img src="images/logo/logo_3.png" alt="" /></a></li>-->

                                                    <li><a href="#" onclick="return addLogoSvg('0138');"><img
                                                                    src="images/clipart/0138.svg" alt=""/></a></li>
                                                    <li><a href="#" onclick="return addLogoSvg('0150');"><img
                                                                    src="images/clipart/0150.svg" alt=""/></a></li>
                                                    <li><a href="#" onclick="return addLogoSvg('0192');"><img
                                                                    src="images/clipart/0192.png" alt=""/></a></li>
                                                    <li><a href="#" onclick="return addLogoSvg('0196');"><img
                                                                    src="images/clipart/0196.png" alt=""/></a></li>
                                                    <li><a href="#" onclick="return addLogoSvg('0046');"><img
                                                                    src="images/clipart/0046.png" alt=""/></a></li>
                                                    <li><a href="#" onclick="return addLogoSvg('0020');"><img
                                                                    src="images/clipart/0020.png" alt=""/></a></li>

                                                </ul>
                                            </div>

                                        </div>
                                    </div>
                                </div>

                                <div class="panel panel-default">
                                    <div class="panel-heading green_back">
                                        <h4 class="panel-title">
                                            <a data-toggle="collapse" data-parent="#accordion" href="#collapse3">
                                                <i class="fa fa-star"></i> Shapes
                                            </a>
                                        </h4>
                                    </div>
                                    <div id="collapse3" class="panel-collapse collapse">
                                        <div class="panel-body">

                                            <div class="element-area clrboth list-col-4">
                                                <!--<input type="button" value="addrect" onclick="addrect()">-->

                                                @if($environment == 'development')
                                                    <input type="button" value="undo" onclick="undo()">
                                                    <input type="button" value="redo" onclick="redo()">

                                            @endif
                                            <!--<input type="button" value="clear" onclick="clearcan()">-->
                                                <input class="form-control" id="shape_color_code" type="color"
                                                       name="shape_color" value="#000000"
                                                       onchange="changeFillColor('shape');">

                                                <ul>
                                                    <li><a href="#" onclick="return addShape('rectangle');"><img
                                                                    src="images/clipart/0008.png" alt=""/></a></li>
                                                    <li><a href="#" onclick="return addShape('triangle');"><img
                                                                    src="images/clipart/0008.png" alt=""/></a></li>
                                                    <li><a href="#" onclick="return addShape('circle');"><img
                                                                    src="images/clipart/0008.png" alt=""/></a></li>
                                                    <li><a href="#" onclick="return addShape('star');"><img
                                                                    src="images/clipart/0008.png" alt=""/></a></li>
                                                    <li><a href="#" onclick="return addShape('polygon');"><img
                                                                    src="images/clipart/0008.png" alt=""/></a></li>

                                                </ul>
                                            </div>

                                        </div>
                                    </div>
                                </div>

                                <div class="panel panel-default">
                                    <div class="panel-heading green_back">
                                        <h4 class="panel-title">
                                            <a data-toggle="collapse" data-parent="#accordion" href="#collapse4">
                                                <i class="fa fa-clipboard"></i> Clipart
                                            </a>
                                        </h4>
                                    </div>
                                    <div id="collapse4" class="panel-collapse collapse">
                                        <div class="panel-body">

                                            <div class="form-group col-sm-6 p0">
                                                <input class="form-control" id="svg_color_code" type="color"
                                                       name="svg_color" value="#000000"
                                                       onchange="changeFillColor('svg');">
                                                {{--<select class="form-control">--}}
                                                    {{--<option value="abc">Cars</option>--}}
                                                    {{--<option value="abc" disabled>Bikes</option>--}}
                                                {{--</select>--}}
                                            </div>


                                            <div class="element-area clrboth list-col-4">
                                                <ul>

                                                    <li><a href="#" onclick="return addClipArtImage('0008');"><img
                                                                    src="images/clipart/0008.png" alt=""/></a></li>
                                                    <li><a href="#" onclick="return addClipArtImage('0035');"><img
                                                                    src="images/clipart/0035.png" alt=""/></a></li>

                                                    <!--<li><a href="#" onclick="return addClipArtImage('0046');"><img src="images/clipart/0046.png" alt="" /></a></li>-->

                                                    <li><a href="#" onclick="return addClipArtImage('0076');"><img
                                                                    src="images/clipart/0076.png" alt=""/></a></li>
                                                    <li><a href="#" onclick="return addClipArtImage('0134');"><img
                                                                    src="images/clipart/0134.png" alt=""/></a></li>
                                                    <li><a href="#" onclick="return addClipArtImage('0169');"><img
                                                                    src="images/clipart/0169.png" alt=""/></a></li>
                                                    <li><a href="#" onclick="return addClipArtImage('0190');"><img
                                                                    src="images/clipart/0190.png" alt=""/></a></li>

                                                </ul>
                                            </div>

                                        </div>
                                    </div>
                                </div>
                        @endif

                                @foreach($customize_items as $key => $value)


                                    <div class="panel panel-default">
                                        <div class="panel-heading green_back">
                                            <h4 class="panel-title">
                                                <a data-toggle="collapse" data-parent="#accordion" href="#{{ 'collapse_'.preg_replace('/\s+/', '', $key) }}">
                                                    <i></i> {{ $key }}
                                                </a>
                                            </h4>
                                        </div>
                                        <div id="{{ 'collapse_'.preg_replace('/\s+/', '', $key) }}" class="panel-collapse collapse">
                                            <div class="panel-body">

                                                <div class="element-area clrboth list-col-4">
                                                    <ul>

                                                        @foreach($value as $items)
                                                        <li><a href="#" onclick="return addItemImage('{{ $items->image }}')"><img
                                                                        src="{{ asset('uploads/tool_items')."/".$items->image }}" alt=""/></a></li>
                                                            @endforeach

                                                    </ul>
                                                </div>

                                            </div>
                                        </div>
                                    </div>


                                @endforeach




                                <div class="panel panel-default">
                                    <div class="panel-heading green_back">
                                        <h4 class="panel-title">
                                            <a data-toggle="collapse" data-parent="#accordion" href="#collapse5">
                                                <i></i> Upload Image
                                            </a>
                                        </h4>
                                    </div>
                                    <div id="collapse5" class="panel-collapse collapse">
                                        <div class="panel-body">


                                            <div class="file-drop-area fnc-uplaod">
                                                <span class="fake-btn green_back">Upload Image</span>
                                                <!--<span class="file-msg js-set-number">or drag and drop files here</span>-->
                                                <input class="file-input" type="file" id="imgLoader">
                                            </div>

                                            <p>You can choose the file format JPG,PNG or SVG file no larger than 1MB</p>

                                            <!--<div class="form-group col-sm-12 p0">-->
                                            <!--<button class="btn green_back">Upload</button>-->
                                            <!--</div>-->

                                        </div>
                                    </div>
                                </div>


                            </div>


                        </div>
                        <div class="tab-pane active" id="tab2">


                            <div class="text-area bg-silver ">
                                <!--<div class="hed green_back clrhm white p10">-->
                                <!--<h4>Text Here</h4>-->
                                <!--</div>-->

                                <div class="form-group">
                                    <textarea id="my_text" name="add_text2" class="form-control bg-gray"
                                              placeholder="Your Text Here"></textarea>
                                </div>
                            </div>
                            <div class="font-area fc30x30 bg-white col-xs-12 pt10 pb10  control-group inline-control">


                                <select class="form-control" id="font_selector" onchange="clickListener(this)">
                                    <option value="Times New Roman">Times New Roman</option>
                                    <option value="Currier New">Currier New</option>
                                    <option value="Tahoma">Tahoma</option>
                                    <option value="arial">Arial</option>
                                    <option value="helvetica" selected="">Helvetica</option>
                                    <option value="myriad pro">Myriad Pro</option>
                                    <option value="delicious">Delicious</option>
                                    <option value="verdana">Verdana</option>
                                    <option value="georgia">Georgia</option>
                                    <option value="courier">Courier</option>
                                    <option value="comic sans ms">Comic Sans MS</option>
                                    <option value="plaster">Plaster</option>
                                    <option value="impact">Impact</option>
                                    <option value="monaco">Monaco</option>
                                    <option value="optima">Optima</option>
                                    <option value="hoefler text">Hoefler Text</option>
                                    <option value="plaster">Plaster</option>
                                    <option value="engagement">Engagement</option>
                                    <span class="caretOne"></span>
                                </select>


                                <input class="form-control" id="text_color_code" type="color" name="text_color"
                                       value="#ffffff" onchange="clickListener(this)">
                                <input class="form-control" id="size_selector" type="number" name="text_size" value="20"
                                       onchange="clickListener(this)">
                                <button id="btn_addtext" class="btn green_back" onclick="AddText()">Add Text</button>

                            </div>
                            <div class="stroke-area fc30x30">
                                <div class="stroke-box col-xs-4 control-group inline-control">
                                    <h5>Stroke</h5>
                                    <button id="btn_stroke_toggle" class="form-control" type="button"
                                            name="stroke_toggle" onclick="clickListener(this)"><i
                                                class="fa fa-paint-brush"></i></button>
                                    <input id="stroke_color_code" class="form-control non-ie" type="color"
                                           name="background_color" value="#01bbd4" onchange="setStrokeColor()" />
									
					<div class="only-ie form-control color-selector0">
    <input type="text" name="clr2" value="" style="display:none"/>
    <button class="iecolorpicker" onclick="var s = Dlg2.ChooseColorDlg(clr2.value); window.event.srcElement.style.backgroundColor = s; clr2.value = s"></button>
    <object id="Dlg2" classid="CLSID:3050F819-98B5-11CF-BB82-00AA00BDCE0B" width="0" height="0"></object></div>

                                    <input id="stroke_size_selector" class="form-control" type="number" name="align"
                                           value="1" onchange="setStrokeSize()">
                                </div>

                                <div class="background-box col-xs-4 control-group inline-control p0">
                                    <h5>Background color</h5>
									<div class="cntr-all">
                                    <div class="form-group hidden-checkbox pul-lft">
                                        <input type="checkbox" id="text_bg_toggle" class="is-changed is-valid non-ie" />
                                        {{--<div class="checkylbl ">--}}
                                        {{--<label for="toggle" class="green_back"></label>--}}
                                        {{--</div>--}}
                                    </div>
                                   
								<input id="text_bg_color_code" class="form-control non-ie" type="color" name="background_color" value="#01bbd4" onchange="clickListener(this)" />
										   
								<span class="only-ie form-control color-selector">
								<input type="text" name="clr" value="" style="display:none"/>
								<button class="iecolorpicker" onclick="var s = Dlg2.ChooseColorDlg(clr.value); window.event.srcElement.style.backgroundColor = s; clr.value = s"></button>
								<object id="Dlg" classid="CLSID:3050F819-98B5-11CF-BB82-00AA00BDCE0B" width="0" height="0"></object></span>
								
									</div>
                                </div>

                                {{--<div class="align-box col-xs-4 control-group inline-control">--}}
                                {{--<h5>Align</h5>--}}
                                {{--<button id="btn_align_left" class="form-control" type="button" name="stroke"><i class="fa fa-align-left"></i></button>--}}
                                {{--<button id="btn_align_center" class="form-control" type="button" name="stroke"><i class="fa fa-align-center"></i></button>--}}
                                {{--<button id="btn_align_right" class="form-control" type="button" name="stroke"><i class="fa fa-align-right"></i></button>--}}
                                {{--</div>--}}

                            </div>

                            <div class="style-area fc30x30 black bg-white  col-xs-12 pb10 pl0">

                                <div class="col-xs-8 inline-control">
                                    <h5>Style</h5>
                                    <button title="Bold" id="btn_bold" class="form-control" type="button" name="stroke"
                                            onclick="clickListener(this)">
                                        <i class="fa fa-bold"></i></button>
                                    <button title="Italic" id="btn_italic" class="form-control" type="button"
                                            name="stroke" onclick="clickListener(this)">
                                        <i class="fa fa-italic"></i></button>
                                    <button title="Underline" id="btn_underline" class="form-control" type="button"
                                            name="stroke" onclick="clickListener(this)">
                                        <i class="fa fa-underline"></i></button>
                                    <button title="Shadow" id="btn_shadow" class="form-control" type="button"
                                            name="stroke" onclick="clickListener(this)">
                                        <i class="fa fa-adjust"></i></button>
                                    <button title="Overline" id="btn_overline" class="form-control" type="button"
                                            name="stroke" onclick="clickListener(this)">
                                        <i class="fa fa-header"></i></button>
                                    <button title="Line Through" id="btn_linethrough" class="form-control" type="button"
                                            name="stroke" onclick="clickListener(this)">
                                        <i class="fa fa-strikethrough"></i></button>
                                </div>

                            </div>


                        </div>
                        <div class="tab-pane" id="tab3">

                            <div class="panel-group" id="accordion2">

                                <div class="panel active panel-default">
                                    <div class="panel-heading green_back">
                                        <h4 class="panel-title">
                                            <a data-toggle="collapse" data-parent="#accordion" href="#collapse_save1">
                                                <i class="fa fa-github-alt"></i> My Designs
                                            </a>
                                        </h4>
                                    </div>
                                    <div id="collapse_save1" class="panel-collapse collapse in">
                                        <div class="panel-body">


                                            <div class="element-area clrboth list-col-4">
                                                <ul>
                                                    <li><a href="#"><img src="images/shirt.png" alt=""/></a>
                                                        <span>Design 1</span></li>
                                                    <li><a href="#"><img src="images/shirt.png" alt=""/></a>
                                                        <span>My Shirt  Design</span></li>
                                                    <li><a href="#"><img src="images/shirt.png" alt=""/></a>
                                                        <span>My Shirt  Design</span></li>
                                                    <!--<li><a href="#"><img src="images/shirt.png" alt="" /></a>-->
                                                    <!--<span>My Shirt  Design</span></li>-->
                                                    <!--<li><a href="#"><img src="images/shirt.png" alt="" /></a>-->
                                                    <!--<span>My Shirt  Design</span></li>-->
                                                </ul>
                                            </div>


                                            <div class="form-group">
                                                <button class="btn green_back pul-rgt">Load</button>
                                            </div>


                                        </div>
                                    </div>
                                </div>


                                <div class="panel panel-default">
                                    <div class="panel-heading green_back">
                                        <h4 class="panel-title">
                                            <a data-toggle="collapse" data-parent="#accordion" href="#collapse_save2">
                                                <i class="fa fa-github-alt"></i> Save Design
                                            </a>
                                        </h4>
                                    </div>
                                    <div id="collapse_save2" class="panel-collapse collapse in">
                                        <div class="panel-body">

                                            <div class="form-group">
                                                <input class="form-control" type="text"
                                                       placeholder="Enter your design name"/>
                                            </div>


                                            <div class="form-group">
                                                <textarea class="form-control" type="text"
                                                          placeholder="Your comment note"></textarea>
                                            </div>

                                            <div class="form-group">
                                                <button class="btn green_back pul-rgt" onclick="saveShirtDesign()">
                                                    Save
                                                </button>
                                                <button class="btn green_back pul-rgt" onclick="loadFromJson()">Load
                                                    Image
                                                </button>
                                            </div>

                                        </div>
                                    </div>
                                </div>
                            </div>


                        </div>

                        <form id="form_add_to_cart" class="prod-detail-form" action="../cart/addcustomizeproduct"
                              method="post">

						<div class="prodcolors fom-bdr / col-xs-12 bdr1 p10 mt-1">
							<div class="text-center"><h5>Colors</h5></div>
							
							<div class="list-col-4 "><ul>
							
                            <?php
                            if (!empty($availabeInColors)) {

                            foreach ($availabeInColors as $color) {
                            ?>
						
                            <li class="prod__radio di black">
                                <input onclick="changeColor(), changePrice('attributes_14')"
                                       id="attributes_14_<?php echo $color->name; ?>" type="radio"
                                       name="attributes[14][]" class="color <?php echo $color->name; ?>"
                                       value="<?php echo $color->name; ?>_option_<?php echo $color->image; ?>_option_<?php echo $color->price; ?>"/>

                                <label for="attributes_14_<?php echo $color->name; ?>"><?php echo $color->name; ?></label>
                            </li>
                            <?php
                            }
                            }
						
                            echo "</ul></div><div class='clearfix'></div> <hr>";
                          
						  echo "<div class='clrlist di'><ul><li>";
                       
                            foreach ($attributes as $attribute) {

                            if ($attribute->type == 'dropdown') {
                            echo "<label>$attribute->name</label>";
                            ?>

                            <select  class="form-control wa" onchange="changePrice('attributes_<?php echo $attribute->attribute_id ?>');"
                                    id="attributes_<?php echo $attribute->attribute_id ?>"
                                    name="attributes[<?php echo $attribute->attribute_id ?>][]">
                               
                                <?php
                                $values = explode(',', $attribute->value_names);
                                sort($values);
                                $value_ids = explode(',', $attribute->value_id);
                                $value_prices = explode(',', $attribute->value_price);
                                while (list($key, $value) = each($values) and list($vkey, $value_id) = each($value_ids) and list($pkey, $value_price) = each($value_prices)) {
                                ?>
                                <option value="<?php echo $value; ?>_option_<?php echo $value_id; ?>_option_<?php echo $value_price; ?>"><?php echo $value; ?></option>
                                <?php
                                }
                                ?>
                            </select>
                            <?php
                            } elseif ($attribute->type == 'textfield') {
                            echo "<label>Enter " . $attribute->name . "</label>";
                            ?>
                            <input value="" name="attributes[<?php echo $attribute->attribute_id ?>][]"
                                   id="attributes_<?php echo $attribute->attribute_id ?>"/>

                            <?php
                            }
                            }
                            ?>

						</li>
                        <li>  <label>Quantity</label>
                            <input value="1" name="quantity" id="quantity" class="form-control wa" />
						</li>
						<li>
                            <!--input class="btn pul-rgt green_back" type="submit" onclick="renderImageForCart();"
                                   id="btn_add_to_cart1" value="Add to Cart"/>-->
								   <button class="btn pul-rgt green_back" type="submit" onclick="renderImageForCart();"
                                   id="btn_add_to_cart1"><i class="fa fa-cart-plus"></i> Add to Cart</button>
						</li>

                            {{--<input type="button" id="btn_add_to_cart" value="Add to Cart old" style="border-radius:2px;" />--}}
                            <input type="hidden" name="total_price" id="total_price" value="<?php echo $price; ?>"/>
                            <input type="hidden" name="price" id="price" value="<?php echo $price; ?>"/>
                            <input type="hidden" name="_token" value="{{ csrf_token() }}">
                            <input type="hidden" name="product_id" id="product_id" value="<?php echo $id; ?>"/>
                            <input id="image_data" name="image_data" type="hidden">


                            {{--<img id="image_data_img"  width="2000px" height="2000px">--}}
							</ul>
							</div>
						  </div>
                        </form>
                    </div>


                    <!--<div class="social-area white text-center">-->
                    <!--<div class="col-xs-12 inline-control">-->
                    <!--<button title="Reset Tool" class="form-control" type="button" name="stroke">-->
                    <!--<i class="fa fa-refresh"></i>-->
                    <!--<span>reset</span>-->
                    <!--</button>-->

                    <!---->
                    <!--<button title="Share" class="form-control" type="button" name="stroke">-->
                    <!--<i class="fa fa-share-alt"></i></button>-->
                    <!--<button class="form-control" type="button" name="stroke">-->
                    <!--<i class="fa fa-facebook"></i></button>-->
                    <!--<button class="form-control" type="button" name="stroke">-->
                    <!--<i class="fa fa-twitter"></i></button>-->
                    <!--<button class="form-control" type="button" name="stroke">-->
                    <!--<i class="fa fa-google"></i></button>-->
                    <!--<button class="form-control" type="button" name="stroke">-->
                    <!--<i class="fa fa-instagram"></i></button>-->
                    <!---->
                    <!--</div>-->
                    <!---->
                    <!--</div>-->


                    <div class="cart-area bg-white col-xs-12 p10">
                        <button class="btn pul-lft green_back" type="button" name="stroke" onclick="saveImg()">Export
                            Image
                        </button>

                        {{--<button class="btn pul-rgt green_back" type="button" name="stroke" disabled>Add to Cart</button>--}}
                    </div>
					
                </div>


                <div class="main__preview preview-area col-xs-7 pl50">

                    @if($environment == 'development')
                        <div class="shirt-sides clrlist listview">
                            <ul>
                                <li id="btn_shirt_front"><a href="#" onclick="return shirtViews('front');"><img
                                                src="images/shirt-mini.png" alt=""/></a></li>
                                <li class="active" id="btn_shirt_back"><a href="#" onclick="return shirtViews('back');"><img
                                                src="images/shirt-mini.png" alt=""/></a></li>
                            </ul>
                        </div>

                    @endif

                    <div class="preview__img text-center p0">
                        <canvas id="c" class="text-center bdr1" width=665px height=570px
                                style="border:1px solid #eee"></canvas>
                    </div>

                    <!--<div class="preview__cont">-->
                    <!--<canvas id="c" width = 230px height = 400px style="border:3px solid #ffff00"></canvas>-->
                    <!--</div>-->

                    <div class="clearfix"></div>

                    <div class="preview__bottom  text-center p0 bg-white col-xs-12">

                        <div class="preview_tool1 col-xs-4 fc30x30 inline-control p0">
                            <button title="Zoom in" class="form-control" type="button" name="stroke" onclick="ZoomIn()">
                                <i class="fa fa-search-plus"></i></button>
                            <button title="Zoom out" class="form-control" type="button" name="stroke"
                                    onclick="ZoomOut()">
                                <i class="fa fa-search-minus"></i></button>
                            <button title="Reset Zoom" class="form-control" type="button" name="stroke"
                                    onclick="ResetZoom()">
                                <i class="fa fa-random"></i></button>
                            <button title="Panning " class="form-control" type="button" name="stroke" disabled>
                                <i class="fa fa-arrows"></i></button>
                        </div>

                        <div class="preview_tool2 col-xs-3 inline-control fnc-range clrhm black">
                            <h5>Transparency</h5>
                            <!--input id="opacity_range" class="form-control" type="range" value="1"-->

                            <input id="opacity_range" class="form-control" type="range" min="0" max="1" step="0.01"
                                   value="1" onchange="changeOpacity(this.value)">
                            <div class="seekbar-progress hidden">
                                <div role="progressbar" aria-valuemin="0" aria-valuemax="100" aria-valuenow="0"
                                     style="width: 0%;"></div>
                            </div>

                            <script>

                                function qs(s) {
                                    return document.querySelector(s)
                                }

                                var handle = qs('.fnc-range input[type="range"]');
                                var progressbar = qs('.fnc-range div[role="progressbar"]');

                                handle.addEventListener('input', function () {
                                    progressbar.style.width = this.value + '%';
                                    progressbar.setAttribute('aria-valuenow', this.value);
                                });

                            </script>


                        </div>
                        <div class="preview_tool3 col-xs-5  fc30x30 inline-control pr0">

                            <button title="Send Back" id="sendBack" class="form-control" type="button" name="stroke"
                                    onclick="sendBack()">
                                <i class="fa fa-clone"></i></button>
                            <button title="Bring Front" id="sendFront" class="form-control" type="button" name="stroke"
                                    onclick="sendFront()">
                                <i class="fa fa-object-ungroup"></i></button>
                            <button title="Flip Horizontal" id="flipHorizontal" class="form-control" type="button"
                                    name="stroke" onclick="flipHorizontal()">
                                <i class="fa fa-exchange"></i></button>
                            <button title="Flip Vertical" id="flipVertical" class="form-control" type="button"
                                    name="stroke" onclick="flipVertical()">
                                <i class="fa fa-exchange"></i></button>
                            <button title="Duplicate" id="duplicate" class="form-control" type="button" name="stroke"
                                    onclick="duplicateObject()">
                                <i class="fa fa-picture-o"></i></button>
                            <button title="Delete" id="delete" class="form-control" type="button" name="stroke"
                                    onclick="RemoveSelectedItems()">
                                <i class="fa fa-trash"></i></button>

                        </div>


                    </div>


                </div>
            </div>


        </section>

    </main>
	
	
				
<style>
.fnc-range div[role="progressbar"] {
    background-color: #4aa9f7;
    height: 5px;
    position: absolute;
    top: 0px;
}

.fnc-range .seekbar-progress {
    height: 5px;
    position: relative;
    overflow: hidden;
    margin-top: -10px;
    width: 95%;
    border-radius: 5px;
}

.fnc-range input[type="range"] {
    -webkit-appearance: none !important;
    width: 100%;
    height: 6px;
    background-color: #23282e !important;
    border-radius: 15px;
    padding: 0px;
}

.fnc-range input[type="range"]:hover {
  background-color: #b2d5aa;
}

.fnc-range input[type="range"]::-webkit-slider-thumb {
  -webkit-appearance: none !important;
  width: 10px;
  height: 20px;
  /* background-color: #95bd3a !important; */
  background-image: url("images/rang-icon.png");
  border-radius: 4px;
  box-shadow: 0px 0px 3px #3c6d59;
  background-repeat:no-repeat;
  z-index:2;
}

.fnc-range input[type="range"]::-webkit-slider-thumb:hover {
  background-color: #457d66;
}

.fnc-range input[type="range"]::-webkit-slider-thumb:active {
  box-shadow: 0px 0px 1px #3c6d59;
}
</style>
				
<script>

function qs(s) { return document.querySelector(s) }

var handle = qs('.fnc-range input[type="range"]');
var progressbar = qs('.fnc-range div[role="progressbar"]');

handle.addEventListener('input', function(){
  progressbar.style.width = this.value + '%';
  progressbar.setAttribute('aria-valuenow', this.value);
});



if (navigator.appName == 'Microsoft Internet Explorer' ||  !!(navigator.userAgent.match(/Trident/) || navigator.userAgent.match(/rv:11/)) || (typeof $.browser !== "undefined" && $.browser.msie == 1))
{
  jQuery("html").addClass("ie");
}

</script>
				
				
@include('front/product_tool_script')
@endsection
