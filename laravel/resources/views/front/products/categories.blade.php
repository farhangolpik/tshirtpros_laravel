<?php
$currency = Config::get('params.currency');
?> 

@if(count($categories)>0)
@foreach ($categories as $category)

			<div class="feat__box col-sm-3 aaa">
				<div class="feat__inr">
					<div class="feat__img grow">
						<div class="__img">
						<a href="<?php echo url('category/'. $category->id); ?>"><img src="{{ asset('uploads/categories')}}/<?php echo $category->image; ?>" alt="" /></a>
						</div>
					</div>
					<div class="feat__sale">
					  <div class="feat__sale__img ">
							<img src="{{asset('')}}/front/images/sale.png" />
					  </div>
					  <div class="feat__sale__cont">
						<span><p><small>upto</small>10%<p></span>
					  </div>
					</div>
					<div class="feature-item-cont">
						<a href="<?php echo url('category/' . $category->id); ?>"><h3><?php echo $category->name; ?></h3></a>
						<?php //echo $product->teaser; ?>
						<div class="lnk-btn custom-btn">
							<a href="<?php echo url('category/' . $category->id); ?>" class="btn btn-primary">View Products</a>
						</div>
					</div>
				</div>
			</div>
		
		
@endforeach
@else
<div class="bg-warning">Sorry, there is no results for selected Category</div>
@endif




