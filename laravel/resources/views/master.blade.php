<!DOCTYPE html>

{{--{{asset('/front/--}}
{{--')}}--}}

<html lang="en" class="broken-image-checker">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <!--iPhone from zooming form issue (maximum-scale=1, user-scalable=0)-->
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=0"/>
    <title>Home</title>

    <link rel="icon" type="image/png" href="{{asset('/front/images/favicon.png')}}">

    <!-- Bootstrap --><link href="{{asset('/front/css/bootstrap.min.css')}}" rel="stylesheet">

    <link rel="stylesheet" href="{{asset('/front/css/stylized.css')}}">
    <link rel="stylesheet" href="{{asset('/front/style.css')}}">
    <link rel="stylesheet" href="{{asset('/front/css/colorized.css')}}">
    <link rel="stylesheet" href="{{asset('/front/css/animate.css')}}">
    <link rel="stylesheet" href="{{asset('/front/css/slidenav.css')}}">
    <link rel="stylesheet" href="{{asset('/front/css/font-awesome.min.css')}}">

    <!-- jQuery -->
    <!--[if (!IE)|(gt IE 8)]><!-->
    <script src="{{asset('/front/js/jquery-2.2.4.min.js')}}"></script>
    <!--<![endif]-->


    <!--[if lte IE 8]>
    <script src="{{asset('/front/js/jquery1.9.1.min.js')}}"></script>
    <![endif]-->

    <!--browser selector-->
    <script src="{{asset('/front/js/css_browser_selector.js')}}" type="text/javascript"></script>


    <script src="{{asset('/front/js/html5shiv.min.js')}}"></script>
    <script src="{{asset('/front/js/respond.min.js')}}"></script>


</head>
<body class="transition nav-plusminus slide-navbar slide-navbar--right">
  <header>
    <!-- top bar section start --->
    <section class="top-bar--section">
      <div class="container pl0">
        <div class="top-bar">
          <div class="top-bar-inner">

            <div class="top-bar--lft">
              <ul>
                <li><a href="#"><i class="fa fa-mobile fa-2x"></i> <span>+1 (000) 123-4567</span></a></li>
              </ul>
            </div>

            <div class="top-bar--rgt clrlist">
              <ul>
                <li><a href="contact.php">Contact Us</a></li>
				<li><a href="cart.php">My Account</a></li>
                <li><a href="#">My Whishlist</a></li>
                <li><a href="#">Sign In</a></li>
				<li class="dropdown">
                  <a href="#" class="dropdown-toggle" data-toggle="dropdown">Us Dollar</a>
                  <ul class="dropdown-menu top">
                    <li><a href="#">USD $</a></li>
                    <li><a href="#">EURO &euro;</a></li>
                  </ul>
                </li>
				
              </ul>

            </div>


          </div>
        </div>
      </div>

    </section>

    <!-- top bar section end --->


    <section class="nav-hdr--middle">
      <div class="container pl0 pr0">
        <div class="nav-hdr--middle col-sm-12">
          <div class="nav__hdr__middle__inner">
            <div class="col-sm-3 ml20 logo-area">
              <a class="navbar-brand" href="index.php"><img src="images/logo.png" alt="logo" class="broken-image" /></a>
            </div>

            <div class="search-btn col-sm-6 ml40 mt10">
              <input type="text" name="search" placeholder="Search Entire Store Here.....">
              <button href="#">SEARCH</button>
            </div>

            <div class="shpng-cart col-sm-2 pr0 ml30 mt10">
              <ul>
                <li><i class="fa fa-cart fa-3x" aria-hidden="true"></i><span>0</span></li>
                <li>
                  <ul>
                    <li>
                      <p class="mb0">shopping cart</p>
                    </li>
                    <li>
                      <p class="mb0">$ 0.00</p>
                    </li>
                  </ul>
                </li>
              </ul>

            </div>


          </div>

        </div>
      </div>
      </div>
    </section>


    <!-- header section start -->


    <section class="hdr-area hdr-nav hdr--sticky cross-toggle bg-white" data-navitemlimit="7">
      <div class="container pl0 pr0">
        <nav class="navbar navbar-default" role="navigation" id="slide-nav">
          <div class="container-fluid pr0">
            <!-- Brand and toggle get grouped for better mobile display -->
            <div class="navbar-header">
              <button type="button" class="navbar-toggle collapsed slide-navbar" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
				<span class="sr-only">Toggle navigation</span>
				<span class="icon-bar"></span>
				<span class="icon-bar"></span>
				<span class="icon-bar"></span>
			  </button>

            </div>

            <!-- Collect the nav links, forms, and other content for toggling -->
            <div id="slidemenu">
              <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                <ul class="nav navbar-nav navbar-main navbar-move">
                  <li><a href="index.php">Home</a></li>
                  <li><a href="categories.php">Brands</a></li>
                  <li><a href="">Custom Apparel</a></li>
                  <li><a href="">Locations</a></li>
                  <li><a href="">Custom T-Shirts</a></li>
                  <li><a href="">Custom Hoodies</a></li>
                  <li><a href=""> Custom Ladies Apparel</a></li>
                </ul>

              </div>
              <!-- /.navbar-collapse -->
            </div>
          </div>
          <!-- /.container-fluid -->
        </nav>
      </div>
    </section>
  </header>

  <!-- header section start -->

  <!-- slider section start -->


  <main id="page-content">

    <section class="slider-area hover-ctrl fadeft">
  <div id="carousel-example-generic" class="carousel slide slider--pauseplay" data-ride="carousel" data---interval="false">

    <ol class="carousel-indicators thumbs">
      <li data-target="#carousel-example-generic" data-slide-to="0" class="active"></li>
      <li data-target="#carousel-example-generic" data-slide-to="1"></li>
      <li data-target="#carousel-example-generic" data-slide-to="2"></li>
    </ol>

    <div class="carousel-inner">
      <div class="item active bg-cvr anime-down" style="background-image:url('images/banner-img.jpg');">
        <div class="caro-caps0">
          <div class="container">
            <div class="slide__cont col-sm-7 valigner ml10 anime-up delay1s">
              <div class="valign">
                <h1>DESIGN CUSTOM <span class="clr-shrt--bg">T-SHIRTS</span></h1>
                <h2>Meet your new favorite tee choose from 100s of styles.
	       Free Shipping US Wide, Order Yours Today</h2>
                <ul>
                  <li>T-shirt</li>
                  <li>Tanks</li>
                  <li>Hoodies</li>
                </ul>
                <div class="lnk-btn view-btn">
                  <button href="#">View Store</button>
                </div>

              </div>
            </div>


          </div>
        </div>
      </div>
    </div>

    <!--  <div class="carousel-inner">
		<div class="item active">
		  <img src="images/banner-img.jpg" alt="...">
		  <div class="container posrel">
			  <div class="caro-caps">
				<h1>DESIGN CUSTOM <span class="clr-shrt--bg">T-SHIRTS</span></h1>
				<h2>Meet your new favorite tee choose from 100s of styles.
					Free Shipping US Wide, Order Yours Today</h2>
				<ul>
				<li>T-shirt</li>
				<li>Tanks</li>
				<li>Hoodies</li>
				</ul>
				<div class="lnk-btn view-btn">
				<button href="#">View Store</button>
				</div>
			  </div>
		  </div>
		</div>

	  </div> -->
    <!--
	  <a class="left carousel-control" href="#carousel-example-generic" data-slide="prev">
		<span class="glyphicon glyphicon-chevron-left"></span>
	  </a>
	  <a class="right carousel-control" href="#carousel-example-generic" data-slide="next">
		<span class="glyphicon glyphicon-chevron-right"></span>
	  </a> -->


  </div>

</section>


<!-- slider section end -->

<!-- slider bottom area start -->

<section class="slider-bg btm-section">
  <div class="container">
    <div class="slider-bg btm-area col-sm-12">
      <div class="slider__bg btm__area__inner">

        <div class="slider__btm__cont col-sm-8">
          <div class="slider__btm__cont__inner">
            <p>Have a great idea? T-shirtPros makes your design easily.</p>
          </div>
        </div>

        <div class="lnk-btn design-btn col-sm-3">
			<a href="panel.php">START DESIGNING</a>
        </div>


      </div>
    </div>
  </div>
</section>


<!-- slider bottom area end -->

<!-- about section START -->

<section class="abt-us-section" style="background-image:url('images/abt-bg.jpg');">
  <div class="container">
    <div class="abt-area">
      <div class="abt__area__inner">


        <div class="abt-cont-area col-sm-7">
          <div class="abt__cont__inner">
            <div class="hed zigzag text-left"><h2>About us</h2></div>
            
            <p>When it comes to T-shirtPros, there's no denying it: you're obsessed. We understand how important your T-shirt project is to you. That's why we've established exactingstandards for print quality and offer multiple printing technologies to
              ensure that your order will come out great. No matter how your T-shirts are printed, They are sure to bring your group together. Custom T-shirts have the power to create lasting memories, start new traditions, and unite groups for a common
              purpose. We’ll take care of the printing so you and your group can show off your awesome T- shirts together!</p>

          </div>
        </div>

        <div class="abt-item-img col-sm-5">
          <div class="abt__item__img__inner">
            <img src="images/abt-img.png" />
          </div>
        </div>


      </div>
</section>

<!-- about section end -->

<!-- feature item start -->

<section class="feature-item-section">
  <div class="container">
    <div class="feature-item">
      <div class="feature__item__inner">

        <div class="hed zigzag">
          <h2>Featured Items</h2>
        </div>

        <div class="feature-item-sub-title">
          <p>The easiest way to order custom t-shirts online</p>
        </div>

        <div class="feat__box col-sm-3">
          <div class="feat__inr">
            <div class="feat__img grow">
				<div class="__img">
					<img src="images/feature-item1.jpg" />
				</div>
            </div>
            <div class="feat__sale">
              <div class="feat__sale__img ">
					<img src="images/sale.png" />
			  </div>
              <div class="feat__sale__cont">
                <span><p><small>upto</small>10%<p></span>
              </div>
            </div>
            <div class="feature-item-cont">
              <h1>Design Your Own T-Shirt</h1>
              <div class="price">$ 49.99 <span class="old-price">$ 59.99</span></div>
            </div>
            <div class="lnk-btn custom-btn">
              <a href="product-detail.php">CUSTOMIZED IT</a>
            </div>
          </div>
        </div>


        <div class="feat__box col-sm-3">
          <div class="feat__inr">
            <div class="feat__img grow">
				<div class="__img">
              <img src="images/feature-item2.jpg" />
				</div>
            </div>
            <div class="feature-item-cont">
              <h1>Design Your Own T-Shirt</h1>
              <div class="price">$ 49.99 <span class="old-price">$ 59.99</span></div>
            </div>
            <div class="lnk-btn custom-btn">
              <button>CUSTOMIZED IT</button>
            </div>
          </div>
        </div>

        <div class="feat__box col-sm-3">
          <div class="feat__inr">
            <div class="feat__img grow">
				<div class="__img">
              <img src="images/feature-item3.jpg" />
				</div>
            </div>
            <div class="feat__sale">
              <div class="feat__sale__img">
                <img src="images/sale.png" />
              </div>
              <div class="feat__sale__cont">
                <span><p><small>upto</small>10%<p></span>
              </div>
            </div>
            <div class="feature-item-cont">
              <h1>Design Your Own T-Shirt</h1>
              <div class="price">$ 49.99 <span class="old-price"></span></div>
            </div>
            <div class="lnk-btn custom-btn">
              <button>CUSTOMIZED IT</button>
            </div>
          </div>
        </div>

        <div class="feat__box col-sm-3">
          <div class="feat__inr">
            <div class="feat__img grow">
				<div class="__img">
					<img src="images/feature-item4.jpg" />
				</div>
            </div>
            <div class="feature-item-cont">
              <h1>Design Your Own T-Shirt</h1>
              <div class="price">$ 13.99 <span class="old-price">$ 30.99</span></div>
            </div>
            <div class="lnk-btn custom-btn">
              <button>CUSTOMIZED IT</button>
            </div>
          </div>
        </div>

        <div class="feat__box col-sm-3">
          <div class="feat__inr">
            <div class="feat__img grow">
				<div class="__img">
					<img src="images/feature-item5.jpg" />
				</div>
            </div>
            <div class="feat__sale">
              <div class="feat__sale__img">
                <img src="images/sale.png" />
              </div>
              <div class="feat__sale__cont">
                <span><p><small>upto</small>10%<p></span>
              </div>
            </div>
            <div class="feature-item-cont">
              <h1>Design Your Own T-Shirt</h1>
              <div class="price">$ 49.99 <span class="old-price">$ 59.99</span></div>
            </div>
            <div class="lnk-btn custom-btn">
              <button>CUSTOMIZED IT</button>
            </div>
          </div>
        </div>

        <div class="feat__box col-sm-3">
          <div class="feat__inr">
            <div class="feat__img grow">
				<div class="__img">
					<img src="images/feature-item6.jpg" />
				</div>
            </div>
            <div class="feature-item-cont">
              <h1>Design Your Own T-Shirt</h1>
              <div class="price">$ 13.99 <span class="old-price">$ 30.99</span></div>
            </div>
            <div class="lnk-btn custom-btn">
              <button>CUSTOMIZED IT</button>
            </div>
          </div>
        </div>


        <div class="feat__box col-sm-3">
          <div class="feat__inr">
            <div class="feat__img grow">
				<div class="__img">
					<img src="images/feature-item7.jpg" />
				</div>
            </div>
            <div class="feat__sale">
              <div class="feat__sale__img">
                <img src="images/sale.png" />
              </div>
              <div class="feat__sale__cont">
                <span><p><small>upto</small>10%<p></span>
              </div>
            </div>
            <div class="feature-item-cont">
              <h1>Design Your Own T-Shirt</h1>
              <div class="price">$ 20.00 <span class="old-price"></span></div>
            </div>
            <div class="lnk-btn custom-btn">
              <button>CUSTOMIZED IT</button>
            </div>
          </div>
        </div>

        <div class="feat__box col-sm-3">
          <div class="feat__inr">
            <div class="feat__img grow">
				<div class="__img">
					<img src="images/feature-item8.jpg" />
				</div>
            </div>
            <div class="feature-item-cont">
              <h1>Design Your Own T-Shirt</h1>
              <div class="price">$ 13.99 <span class="old-price">$ 30.99</span></div>
            </div>
            <div class="lnk-btn custom-btn">
              <button>CUSTOMIZED IT</button>
            </div>
          </div>
        </div>
      </div>
    </div>
</section>


<section class="custom-apparel" style="background:url('images/custom-bg.jpg')">
  <div class="container">
    <div class="custom">
      <div class="custom__inr">

        <div class="hed black text-center zigzag color-invert">
          <h2>Source of Custom Apparel</h2>
        </div>

        <div class="custom-sub-title">
          <p>The easiest way to order custom t-shirts online</p>
        </div>
      </div>
    </div>

    <div class="custom-item col-sm-12">
      <div class="item__inr">

        <div class="item1-img col-sm-4">
          <img src="images/apparel-item1.png" />
        </div>

        <div class="item1-desc col-sm-4">
          <div class="item1__desc__inner">
            <div class="item1__desc__title">
              <h1>01</h1>
              <h2>Choose a Product</h2>
            </div>
            <div class="item1__desc__cont">
              <p>We have 100+ different products to choose from including t-shirts, tank tops, hoodies, long sleeves, jerseys and more.</p>
            </div>
          </div>
        </div>

      </div>
    </div>


    <div class="custom-item col-sm-5 col-sm-offset-7">
      <div class="item__inr">

        <div class="item2-desc col-sm-12">
          <div class="item2__desc__inner">
            <div class="item2__desc__title">
              <h1>02</h1>
              <h2>CUSTOMIZED <br/>iT!</h2>
            </div>
            <div class="item2__desc__cont">
              <p>Upload your own artwork or create a design online. Our designer makes it really easy to create custom apparel.</p>
            </div>
          </div>
        </div>

        <div class="item2-img col-sm-10 col-sm-offset-2">
          <img src="images/apparel-item2.png" />
        </div>


      </div>
    </div>

    <div class="custom-item col-sm-12">
      <div class="item__inr">

        <div class="item3-img col-sm-12">
          <img src="images/apparel-item3.png" />
        </div>

        <div class="item3-desc col-sm-12">
          <div class="item3__desc__inner">
            <div class="item3__desc__title col-sm-2 pr0 pl0">
              <h1>03</h1>
              <h2>CHECKOUT</h2>
            </div>
            <div class="item3__desc__cont col-sm-4 pl0">
              <p>Your order will be printed in US with the highest standards for quality, and the delivery date is fully guaranteed!</p>
            </div>
          </div>
        </div>
      </div>
    </div>

  </div>
</section>

    <footer>

  <section class="ftr-top col-sm-12 p0">
    <div class="container">

          <div class="ftr-top-item col-sm-4">
            <div class="ftr__top__item__inr clrlist text-center">

              <ul>
                <li> <img src="images/stndrd-shipng.png" /> </li>
                <li>
                  <h3> standard shipping </h3></li>
                <li> Normally in 14 Business Days </li>
                <li> FREE SHIPPING on orders over $100 </li>
              </ul>

            </div>
          </div>

          <div class="ftr-top-item col-sm-4">
            <div class="ftr__top__item__inr clrlist text-center">

              <ul>
                <li> <img src="images/rush-shipng.png" /> </li>
                <li>
                  <h3> RUSH SHIPPING </h3></li>
                <li> 7 Business Days </li>
                <li> 7 DAYS Faster </li>
              </ul>


            </div>
          </div>

          <div class="ftr-top-item col-sm-4">
            <div class="ftr__top__item__inr clrlist text-center">

              <ul>
                <li> <img src="images/support.png" /> </li>
                <li>
                  <h3> ONLINE SUPPORT </h3></li>
                <li> We support online 24 hours a </li>
                <li> day</li>

              </ul>

            </div>
          </div>

    </div>
  </section>


  <section class="ftr-btm col-sm-12 p0">


    <div class="container">


          <div class="ftr__box contact-us col-sm-4">
            <div class="contact__us__inr clrlist">
			<h4>Contact Us</h4> 
              <ul>
                <li>
                  <a> <img src="images/ftr-addres.png" /> <span>Address: 123 PP Street, New York</span> </a>
                </li>
                <li>
                  <a> <img src="images/ftr-phn.png" /> <span>Call Us: +012 345 6789</span> </a>
                </li>
                <li>
                  <a> <img src="images/ftr-msg.png" /> <span>Email Us: Info@Company.com</span> </a>
                </li>
                <li>
                  <ul>
                    <li>
                      <a> <img src="images/fb-icon.png" /></a>
                    </li>
                    <li>
                      <a> <img src="images/Twitter-icon.png" /></a>
                    </li>
                    <li>
                      <a> <img src="images/Gmail-icon.png" /></a>
                    </li>
                    <li>
                      <a> <img src="images/Insta-icon.png" /></a>
                    </li>
                  </ul>
                </li>

              </ul>
            </div>
          </div>


          <div class="ftr__box info col-sm-4">
            <div class="info__inr clrlist">
				<h4>Other Info</h4>
              <ul>
                <li>
                  <p>At T-shirtPros, we're good at two things: Having fun and creating t-shirts. Strong believers in a "work hard, play hard" philosophy, we spend time perfecting our printing processes, bending over backward for our customers</p>
                </li>
                <li><img src="images/cards.png" /></li>
              </ul>

            </div>
          </div>

          <div class="ftr__box newsltr col-sm-4">
            <div class="newsltr__inr clrlist">
				<h4>Subscribe Newsletter</h4>
              <ul>
                <li>
                  <div class="news-btn col-sm-12 pl0">
                    <input type="text" name="search" placeholder="Enter your email.....">
                    <button href="#"><i class="fa fa-paper-plane-o fa-2x" aria-hidden="true"></i></button>
                  </div>
                </li>
            </div>
          </div>

        </div>
     
    </div>
  </section>

  <section class="copyright-area bg-gray white p20 col-sm-12 a">
    <div class="container">

      <div class="ftr-copyright col-sm-12 ">
          <div class="pul-lft">Copyright &#9400; 2017 &nbsp; T-shirt Pros. All Right Reserved.</div>
		  <div class="pul-rgt">Designed & Developed by <span><a href="http://www.golpik.com">GOLPIK</a></span>. </div>
      </div>

    </div>
  </section>

</footer>

</main>

<!--Bootstrap -->
<script type="text/javascript" src="js/bootstrap.min.js"></script>
<!--./Bootstrap -->

<!--Major Scripts-->
<script src="js/viewportchecker.js"></script>
<script src="js/kodeized.js"></script>
<!--./Major Scripts-->


</body>

</html>